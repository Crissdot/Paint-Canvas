$(document).ready(setColors);

function setColors(){
    var colors = ["yellow","pink","orange","gray","red","black","white"];

    for (let i = 0; i < colors.length; i++) {
        $('#changeColor').append('<button class="setColor" id=' + colors[i] + ' style="background-color:' + colors[i] +'"  ></button>')
    }

    $('#changeColor button').click(function(){
        let selectedColor = $(this).attr('id');

        context.fillStyle = selectedColor;
        context.strokeStyle = selectedColor;
    });
}